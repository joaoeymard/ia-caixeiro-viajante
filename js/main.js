var mapa = new Mapa([
  ['a', 7, 'b'], ['a', 5, 'd'],
  ['b', 7, 'a'], ['b', 8, 'c'], ['b', 9, 'd'], ['b', 7, 'e'],
  ['c', 8, 'b'], ['c', 5, 'e'],
  ['d', 5, 'a'], ['d', 9, 'b'], ['d', 15, 'e'], ['d', 6, 'f'],
  ['e', 7, 'b'], ['e', 5, 'c'], ['e', 15, 'd'], ['e', 8, 'f'],
  ['f', 6, 'd'], ['f', 8, 'e'], ['f', 11, 'g'],
  ['g', 11, 'f'], ['g', 9, 'e']
], ['a', 'b', 'c', 'd', 'e', 'f', 'g'], 'e', 'g');

var populacao = new Populacao();

const listCidades = mapa.getMapa().cidades;
populacao.gerarPopulacao(20, 7, listCidades);
var algoritmo = new Algoritmo();
var result = [];
var dataChart = {
  datasets: [{
    backgroundColor: 'transparent',
		borderColor: 'rgb(54, 162, 235)',
    label: 'Melhor Fitness',
    data: []
  }, {
    backgroundColor: 'transparent',
    borderColor: 'rgb(255, 99, 132)',
    label: 'Média de Fitness',
    data: []
  }],
  labels: []
}

algoritmo.aptidao(populacao.getPopulacao(), mapa);
do {
  var selecao = algoritmo.selecao();
  var operadores = algoritmo.mutacao( algoritmo.reproducao( selecao ) );

  algoritmo.setPopulacao( operadores );
  algoritmo.aptidao(populacao.getPopulacao(), mapa);

  delete selecao, operadores;

  dataChart.datasets[0].data.push(algoritmo.getFitnessMelhor());
  dataChart.datasets[1].data.push(algoritmo.getFitnessMedia());
  dataChart.labels.push(algoritmo.getGeracaoNum());
  console.log("Geração:", algoritmo.getGeracaoNum());
} while (algoritmo.getGeracaoNum() < 10000 && algoritmo.parada(0.95, 300));

var grafico = new Chart($("#grafico"), {
  type: 'line',
  data: dataChart,
  options: {
  }
});

document.getElementById('preview').innerText += algoritmo.toString() + "\n\n";
